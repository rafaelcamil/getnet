package step_definitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PortabilityTest {
    private static WebDriver driver;
    private final WebDriverWait wait = new WebDriverWait(driver, 30);
    private static JavascriptExecutor js;
    String modalOption = "";

    /**
     * Setup do teste
     */
    @Before
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
    }

    /**
     * Finalização do teste. Encerrando instância do driver
     */
    @After
    public static void tearDown() {
        driver.quit();
    }

    /**
     * Abrindo Homepage e maximizando
     */
    @Given("Homepage")
    public void homepage() {
        driver.manage().window().maximize();
        driver.get("https://site.getnet.com.br/");
    }

    /**
     * Abre o campo para digitação do texto de busca.
     */
    @Given("Click on search field")
    public void clickOnSearchField() {
        wait.until((ExpectedCondition<Boolean>) wd ->
                (js.executeScript("return document.readyState").equals("complete")));

        driver.findElement(By.cssSelector(".c-mobile-header-item__content")).click();
    }

    /**
     * Escreve texto para pesquisa.
     */
    @Given("Write text {string}")
    public void writeText(String search) {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("c-search-content-wrapper"))));
        driver.findElement(By.id("global-search-input")).sendKeys(search);
    }

    /**
     * Efetua pesquisa.
     */
    @Given("Search")
    public void search() {
        driver.findElement(By.className("c-search-box__button")).click();
    }

    /**
     * Percorre páginas verificando se a opção está contida no modal.
     * Seleciona opção a fim de abrir modal.
     */
    @When("Select portability {string}")
    public void selectPortabilityOption(String option) {
        modalOption = option;
        int actualPage = 1;
        boolean notFound = true;
        WebElement modal = null;

        while (notFound) {
            wait.until((ExpectedCondition<Boolean>) wd ->
                    (js.executeScript("return document.readyState").equals("complete")));

            Map<String, WebElement> pages = getPagination();

            List<WebElement> modals = driver.findElement(By.cssSelector("body > div.c-wrapper > div > section")).findElements(By.tagName("a"));

            for (WebElement element : modals) {
                if (element.getText().equals(option)) {
                    modal = element;
                    notFound = false;
                    break;
                }
            }
            if (notFound) {
                actualPage++;
                pages.get(String.valueOf(actualPage)).click();
            }
        }

        Actions builder = new Actions(driver);
        builder.moveToElement(modal).click().perform();
    }

    /**
     * Verifica se o modal aberto está com o título correto.
     */
    @Then("System displays result")
    public void systemDisplaysResult() {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy((By.className("is-modal-open"))));
        WebElement result = driver.findElement(By.className("is-modal-open")).findElement(By.className("o-modal__title"));
        assertEquals(result.getText(), modalOption);
    }

    /**
     * @return paginação contida no modal
     */
    private Map<String, WebElement> getPagination() {
        List<WebElement> listPages = driver.findElement(By.className("o-pagination")).findElements(By.className("page-numbers"));

        Map<String , WebElement> pages = new HashMap<>();
        for(WebElement element : listPages){
            element.getText();
            pages.put(element.getText(),element);
        }
        return pages;
    }
}