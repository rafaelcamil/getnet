# new feature
# Tags: optional
@SignUpFeature
Feature: Portability

  Scenario Outline: Portability message validation
    Given Homepage
    And Click on search field
    And Write text "superget"
    And Search
    When Select portability <option>
    Then System displays result

    Examples:
      | option                             |
      | "Quero adquirir, como devo fazer?" |
#      | "Como faço a portabilidade da minha maquininha?" |